#! /bin/bash
# Kserver
# @edt ASIX M11-SAD Curs 2021-2022

# Ficheros de configuracion
cp /opt/docker/krb5.conf /etc/krb5.conf
cp /opt/docker/kdc.conf  /etc/krb5kdc/kdc.conf
cp /opt/docker/kadm5.acl /etc/krb5kdc/kadm5.acl

# Database
kdb5_util create -s -P masterkey # -P contraseña de la Database (masterkey)

# Creacio Usuaris
for user in pere pau jordi anna marta julia admin user{01..10}
do
	# Creacion Passwords
	kadmin.local -q "addprinc -pw k$user $user"
done

# Crear marta/admin
kadmin.local -q "addprinc -pw kmarta marta/admin"
kadmin.local -q "addprinc -pw kadmin admin"

# Utilizar /etc/passwd como IP (Information Provider)
for user in kuser{01..06}
do
	kadmin.local -q "addprinc -pw $user $user"
done

kadmin.local -q "addprinc -randkey host/sshd.edt.org"

# Encender servers, en detach
/etc/init.d/krb5-admin-server start
/etc/init.d/krb5-kdc start
/etc/init.d/krb5-admin-server status
/etc/init.d/krb5-kdc status

/usr/sbin/krb5kdc
/usr/sbin/kadmind -nofork

/bin/bash