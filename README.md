
- Practica 1:
    - [Imatges Docker](#practica-1-imatges-docker)
    - [Authenticacion](#practica-1-authenticacion)
- Practica 2:
    - [Instal·lació](#practica-2-installacio)
    - [Host aula + Kerberos + AWS EC2](#practica-2-host-aula--kerberos--aws-ec2)
- Practica 3:
    - [Kerberos + LDAP (PAM)](#práctica-3-kerberos--ldap-pam)
    - [Host Aula + Kerberos + LDAP + AWS EC2](#práctica-3-host-aula--kerberos--ldap--aws-ec2)

# Practica 1: Imatges Docker
#### Necesario: kserver
- [x] en detach
- [x] users principales: pau, jordi, anna, marta, marta/admin, julia y admin
- [x] otro users: user01..user06
- [x] nom_host: kserver.edt.org

1. crear el Dockerfile de la images
```dockerfile
FROM debian:11
LABEL version="1.0"
LABEL author="@edt ASIX-M06"
LABEL subject="Kerberos Server"
RUN apt-get update
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get -y install krb5-admin-server procps iproute2 tree nmap vim less finger passwd mlocate nano
# samba samba-client
RUN mkdir /opt/docker
COPY . /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE 88 464 749
```
2. modificar el startup.sh
2.1. pasar los ficheros de configuracion del servidor Kerberos
2.2. crear los usuarios principales y otros
2.3. crear sus passwords
2.4. enceder los servers necesarios, en detach
```bash
#! /bin/bash
# Ficheros de configuracion
cp /opt/docker/krb5.conf /etc/krb5.conf
cp /opt/docker/kdc.conf /etc/krb5kdc/kdc.conf
cp /opt/docker/kadm5.acl /etc/krb5kdc/kadm5.acl

# Database
kdb5_util create -s -P masterkey # -P contraseña de la Database (masterkey)

# Creacion Usuaris
for user in pau jordi anna marta julia admin user{01..06}
do
    # Creacion Passwords
    kadmin.local -q "addprinc -pw k$user $user"
done

# Crear marta/admin
kadmin.local -q "addprinc -pw kmarta marta/admin"
kadmin.local -q "addprinc -pw kadmin admin "

# Utilizar /etc/passwd como IP (Information Provider)
for user in kuser{01..06}
do
  kadmin.local -q "addprinc -pw $user $user"
done

# Encender servers, en detach
/etc/init.d/krb5-admin-server start
/etc/init.d/krb5-kdc start

/usr/sbin/krb5kdc
/usr/sbin/kadmind -nofork

/bin/bash
```
3. configurar los ficheros de configuracion
3.1. krb5.conf
3.2. kdc.conf
3.3. kadm5.acl

krb5.conf
```conf
[realms]
	EDT.ORG = {
		kdc = kserver.edt.org
		admin_server = kserver.edt.org
	}

[domain_realm]
	.edt.org=EDT.ORG
	edt.org=EDT.ORG
```
kdc.conf
```conf
[kdcdefaults]
    kdc_ports = 750,88

[realms]
    EDT.ORG = {
        database_name = /var/lib/krb5kdc/principal
        admin_keytab = FILE:/etc/krb5kdc/kadm5.keytab
        acl_file = /etc/krb5kdc/kadm5.acl
        key_stash_file = /etc/krb5kdc/stash
        kdc_ports = 750,88
        max_life = 10h 0m 0s
        max_renewable_life = 7d 0h 0m 0s
        master_key_type = des3-hmac-sha1
        #supported_enctypes = aes256-cts:normal aes128-cts:normal
        default_principal_flags = +preauth
    }
```
kadm5.acl
Asignamos los roles de admin a pau admin y marta/admin
```conf
*/admin@EDT.ORG *
admin@EDT.ORG *
pau@EDT.ORG *
```
4. pasamos a mostar la imagen
***la IP cambia segun que container abres primero, segurate de apuntar la IP del kserver***
```
$ sudo docker build -t cristiancondolo21/krb22:kserver .
```
5. nombrar imagen (kserver.edt.org) en nuestro /etc/hosts local.
```
<IP_Docker_kserver>     kserver.edt.org
```

#### Necesario: khost
- [x] herramientas: kinit, klist y kdestroy (no PAM)
- [x] servidor: kserver.edt.org
- [x] verificar con kadmin

1. editar el Dockerfile de la imagen
1.2. del Dockerfile del kserver, solamente cambiamos el paquete de instalacion (krb5-user); dentro ya vienen las herramientas
```dockerfile
FROM debian:11
LABEL version="1.0"
LABEL author="@edt ASIX-M06"
LABEL subject="Kerberos Client"
RUN apt-get update
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get -y install krb5-user procps iproute2 tree nmap vim less finger passwd mlocate nano
# samba samba-client
RUN mkdir /opt/docker
COPY . /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE 88 464 749
```
2. editar el starup.sh
2.1. del starupt.sh del kserver, solamente copiamos del krb5.conf
```bash
#! /bin/bash
# Ficheros de configuracion
cp /opt/docker/krb5.conf /etc/krb5.conf

/bin/bash
```
3. configurar para indicar que el servidor Kerberos es kserver.edt.org
```conf
[realms]
	EDT.ORG = {
		kdc = kserver.edt.org
		admin_server = kserver.edt.org
	}

[domain_realm]
	.edt.org=EDT.ORG
	edt.org=EDT.ORG
```

4. pasamos a montar la imagen
```
$ sudo docker build -t cristiancondolo21/krb22:khost .
```
5. arrancamos tanto el kserver como el khost
```
$ sudo docker run --rm --name kserver -h kserver.edt.org --net 2hisix -d cristiancondolo21/krb22:kserver
$ sudo docker run --rm --name khost -h khost.edt.org --net 2hisix -it cristiancondolo21/krb22:khost
```
6. validar que funciones las herramientas
```
@khost # kinit pere
: kpere
@khost # klist
Ticket cache: FILE:/tmp/krb5cc_0
Default principal: pere@EDT.ORG

Valid starting     Expires            Service principal
03/03/22 12:05:32  03/03/22 22:05:32  krbtgt/EDT.ORG@EDT.ORG
	renew until 03/04/22 12:05:29
@khost # kdestroy
@khost # klist
klist: No credentials cache found (filename: /tmp/krb5cc_0)
```
7. valimos que funciona los usuarios con roles admin con la orden kadmin
```
@khost # kadmin -p marta/admin
: kmarta
kadmin:  listprincs    
K/M@EDT.ORG
admin@EDT.ORG
anna@EDT.ORG
host/sshd.edt.org@EDT.ORG
jordi@EDT.ORG
julia@EDT.ORG
kadmin/admin@EDT.ORG
kadmin/changepw@EDT.ORG
kadmin/kserver.edt.org@EDT.ORG
kiprop/kserver.edt.org@EDT.ORG
krbtgt/EDT.ORG@EDT.ORG
kuser01@EDT.ORG
kuser02@EDT.ORG
kuser03@EDT.ORG
kuser04@EDT.ORG
kuser05@EDT.ORG
kuser06@EDT.ORG
marta/admin@EDT.ORG
marta@EDT.ORG
pau@EDT.ORG
pere@EDT.ORG
user01@EDT.ORG
user02@EDT.ORG
user03@EDT.ORG
user04@EDT.ORG
user05@EDT.ORG
user06@EDT.ORG
user07@EDT.ORG
user08@EDT.ORG
user09@EDT.ORG
user10@EDT.ORG
```

# Practica 1: Authenticacion
#### Necessario: khostp
- [x] host con PAM
- [x] servidor: kserver.edt.org
- [x] configura **system-auth** (para Debian: **common-______**) de pam para usar el modulo: ***pam_krb5.so***
- [x] users: local01..local06 y kuser01..kuser06(sin passwd)
- [x] validar usuarios locales (local01..) con pam_unix.so (donde /etc/passwd hace de IP y AP) 
- [x] validar usarios locale+principales (kuser01..) (donde /etc/passwd hace de IP y kerberos hace de AP)

#### Verificación
en una sesion interactiva con le container khostp:
- [x] iniciar con "su -" como local01
- [x] iniciar con "su -" como local02
- [x] iniciar con "su -" como kuser01
- [x] validar que kuser01 obtiene un "ticket" (kinit)...
- [x] ... y que puede acceder con kadmin a la admistración del servidor kerberos (con independencia de los permisos que tenga)

1. copiando los ficheros de la imagen PAM
1.1. ficheros PAM necesarios:
    - common-account
    - common-auth
    - common-passwd
    - common-session
```
$ tree
.
├── common-account
├── common-auth
├── common-password
├── common-session
├── Dockerfile
├── kadm5.acl
├── kdc.conf
├── krb5.conf
└── startup.sh

0 directories, 9 files
```
2. editamos el Dockerfile
2.1. instalar el cliente de kerberos
2.1. instalar el cliente de kerberos
2.2. instalar el pam de kerberos (libpam-krb5)
```Dockerfile
FROM debian:11
LABEL version="1.0"
LABEL author="@edt ASIX-M06"
LABEL subject="Kerberos Client PAM"
RUN apt-get update
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get -y install krb5-admin-server libpam-krb5 mlocate procps iproute2 tree nmap nano vim less finger passwd libpam-pwquality libpam-mount libpam-mkhomedir libpam-ldapd libnss-ldapd nslcd nslcd-utils ldap-utils openssh-client openssh-server 
RUN mkdir /opt/docker
COPY . /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE 88 464 749
```
3. editamos el startup.sh
3.1. añadir los ficheros de configuracion de PAM
3.2. crear los usuario con passwd (local) y los sin passwd (kuser)
```bash
#! /bin/bash
# Khost-PAM
# @edt ASIX M11-SAD Curs 2021-2022

# Ficheros de configuracion
# Kerberos:
cp /opt/docker/krb5.conf /etc/krb5.conf
cp /opt/docker/kdc.conf  /etc/krb5kdc/kdc.conf
cp /opt/docker/kadm5.acl /etc/krb5kdc/kadm5.acl

# PAM:
cp /opt/docker/common-auth /etc/pam.d/common-auth
cp /opt/docker/common-account /etc/pam.d/common-account
cp /opt/docker/common-password /etc/pam.d/common-password
cp /opt/docker/common-session /etc/pam.d/common-session

# Database
kdb5_util create -s -P masterkey # -P contraseña de la Database (masterkey)

# Creacion Grupos
groupadd localU
groupadd kusers

# Creacion Usuarios
useradd -g users -G localU local01
useradd -g users -G localU local02
useradd -g users -G localU local03

useradd -g users -G kusers kuser01
useradd -g users -G kusers kuser02
useradd -g users -G kusers kuser03

# Creacion Passwords
echo -e "local01\nlocal01" | passwd local01
echo -e "local02\nlocal02" | passwd local02
echo -e "local03\nlocal03" | passwd local03

/bin/bash
```
4. configurar los ficheros
4.1. configurar los ficheros de configuracion del kerberos
- krb5.conf
```conf
[realms]
	EDT.ORG = {
		kdc = kserver.edt.org
		admin_server = kserver.edt.org
	}

[domain_realm]
	.edt.org=EDT.ORG
	edt.org=EDT.ORG
```
4.2. configurar los ficheros de configuracion del pam
***El system-auth es de Fedora, para Debian son los fichero common-______***
- common-account
```bash
#
# /etc/pam.d/common-account - authorization settings common to all services
#
# This file is included from other service-specific PAM config files,
# and should contain a list of the authorization modules that define
# the central access policy for use on the system.  The default is to
# only deny service to users whose accounts are expired in /etc/shadow.
#
# As of pam 1.0.1-6, this file is managed by pam-auth-update by default.
# To take advantage of this, it is recommended that you configure any
# local modules either before or after the default block, and use
# pam-auth-update to manage selection of other modules.  See
# pam-auth-update(8) for details.
#

# here are the per-package modules (the "Primary" block)
#account	[success=1 new_authtok_reqd=done default=ignore]	pam_unix.so 

# ===Permisos Host-PAM===================================================
account     sufficient    pam_krb5.so
account     required      pam_unix.so
# =======================================================================

# here's the fallback if no module succeeds
#account	requisite			pam_deny.so

# prime the stack with a positive return value if there isn't one already;
# this avoids us returning an error just because nothing sets a success code
# since the modules above will each just jump around
#account	required			pam_permit.so

# and here are more per-package modules (the "Additional" block)
#account	[success=ok new_authtok_reqd=done ignore=ignore user_unknown=ignore authinfo_unavail=ignore default=bad]	pam_ldap.so minimum_uid=1000
# end of pam-auth-update config
```
- common-auth
```bash
#
# /etc/pam.d/common-auth - authentication settings common to all services
#
# This file is included from other service-specific PAM config files,
# and should contain a list of the authentication modules that define
# the central authentication scheme for use on the system
# (e.g., /etc/shadow, LDAP, Kerberos, etc.).  The default is to use the
# traditional Unix authentication mechanisms.
#
# As of pam 1.0.1-6, this file is managed by pam-auth-update by default.
# To take advantage of this, it is recommended that you configure any
# local modules either before or after the default block, and use
# pam-auth-update to manage selection of other modules.  See
# pam-auth-update(8) for details.

# here are the per-package modules (the "Primary" block)
#auth	[success=2 default=ignore]	pam_unix.so nullok
#auth	[success=1 default=ignore]	pam_ldap.so use_first_pass

#===Autenticacio Host-PAM=============================================
auth        required      pam_env.so
auth        sufficient    pam_unix.so try_first_pass nullok
auth        sufficient    pam_krb5.so 
auth        required      pam_deny.so
#=====================================================================

# here's the fallback if no module succeeds
#auth	requisite			pam_deny.so

# prime the stack with a positive return value if there isn't one already;
# this avoids us returning an error just because nothing sets a success code
# since the modules above will each just jump around
auth	required			pam_permit.so

# and here are more per-package modules (the "Additional" block)
auth	optional	pam_mount.so 
auth	optional			pam_cap.so 
# end of pam-auth-update config
```
- common-password
```bash
#
# /etc/pam.d/common-password - password-related modules common to all services
#
# This file is included from other service-specific PAM config files,
# and should contain a list of modules that define the services to be
# used to change user passwords.  The default is pam_unix.

# Explanation of pam_unix options:
# The "yescrypt" option enables
#hashed passwords using the yescrypt algorithm, introduced in Debian
#11.  Without this option, the default is Unix crypt.  Prior releases
#used the option "sha512"; if a shadow password hash will be shared
#between Debian 11 and older releases replace "yescrypt" with "sha512"
#for compatibility .  The "obscure" option replaces the old
#`OBSCURE_CHECKS_ENAB' option in login.defs.  See the pam_unix manpage
#for other options.

# As of pam 1.0.1-6, this file is managed by pam-auth-update by default.
# To take advantage of this, it is recommended that you configure any
# local modules either before or after the default block, and use
# pam-auth-update to manage selection of other modules.  See
# pam-auth-update(8) for details.

# here are the per-package modules (the "Primary" block)
#password	requisite			pam_pwquality.so retry=3
#password	[success=2 default=ignore]	pam_unix.so obscure use_authtok try_first_pass yescrypt
#password	[success=1 default=ignore]	pam_ldap.so minimum_uid=1000 try_first_pass

#==Passwords Host-PAM=============================================================================
password	requisite	pam_pwquality.so retry=3
password    	sufficient    	pam_unix.so try_first_pass use_authtok nullok sha512 shadow
password    	sufficient    	pam_krb5.so
password    	required      	pam_deny.so
password	requisite	pam_deny.so
password	required	pam_permit.so
password	optional	pam_mount.so disable_interactive
#=================================================================================================

# here's the fallback if no module succeeds
#password	requisite			pam_deny.so

# prime the stack with a positive return value if there isn't one already;
# this avoids us returning an error just because nothing sets a success code
# since the modules above will each just jump around
#password	required			pam_permit.so

# and here are more per-package modules (the "Additional" block)
#password	optional	pam_mount.so disable_interactive
# end of pam-auth-update config
```
- common-session
```bash
#
# /etc/pam.d/common-session - session-related modules common to all services
#
# This file is included from other service-specific PAM config files,
# and should contain a list of modules that define tasks to be performed
# at the start and end of interactive sessions.
#
# As of pam 1.0.1-6, this file is managed by pam-auth-update by default.
# To take advantage of this, it is recommended that you configure any
# local modules either before or after the default block, and use
# pam-auth-update to manage selection of other modules.  See
# pam-auth-update(8) for details.


# here are the per-package modules (the "Primary" block)
#session	[default=1]			pam_permit.so

# here's the fallback if no module succeeds
#session	requisite			pam_deny.so

# prime the stack with a positive return value if there isn't one already;
# this avoids us returning an error just because nothing sets a success code
# since the modules above will each just jump around
#session	required			pam_permit.so

#====Sesions Host-PAM=============================================================
session     	optional	pam_keyinit.so revoke
session     	required      	pam_limits.so
-session     	optional      	pam_systemd.so
session     	[success=1 default=ignore] 	pam_succeed_if.so service in crond quiet use_uid
session 	optional 	pam_mkhomedir.so
session     	sufficient    	pam_krb5.so
session		required	pam_unix.so 
session		optional	pam_mount.so
#=================================================================================

# and here are more per-package modules (the "Additional" block)
#session		required	pam_unix.so 
#session		optional	pam_mount.so
#session 	optional 	pam_mkhomedir.so
#session	[success=ok default=ignore]	pam_ldap.so minimum_uid=1000
# end of pam-auth-update config
```
5. montar la imagen khostp
```
$ sudo docker built -t cristiancondolo21/krb22:khostPAM .
```

#### Veificacion
6. encender los container khostp kserver
```
$ sudo docker run --rm --name kserver -h kserver.edt.org --net 2hisix -d cristiancondolo21/krb22:kserver
$ sudo docker run --rm --name khostPAM -h khostp.edt.org --net 2hisix -it cristiacondolo21/krb22:khostPAM
```

dentro del hostPAM, verificamos que funcione:
7. iniciar con "su -" como local01
```
@khostPAM # su - local01
Creating directory '/home/local01'.
$ 
```
8. iniciar con "su -" como local02
```
@local01 $ su - local02
Password: local02
Creating directory '/home/local02'.
$ 
```
9. iniciar con "su -" como kuser01
```
@local02 $ su - kuser01
Password: 
Password: 
Creating directory '/home/kuser01'.
$
```
10. validar que kuser01 obtiene un "ticket" (kinit)...
```
@ kuser01 $ kinit
@ kuser01 $ klist      
Ticket cache: FILE:/tmp/krb5cc_1003_RiknYq
Default principal: kuser01@EDT.ORG

Valid starting     Expires            Service principal
03/10/22 12:53:01  03/10/22 22:53:01  krbtgt/EDT.ORG@EDT.ORG
	renew until 03/11/22 12:53:01
$ 
```
    10.1. y que puede acceder con kadmin a la admistración del servidor kerberos (con independencia de los permisos que tenga)
```
@ kuser01 $ kadmin
Authenticating as principal kuser01/admin@EDT.ORG with password.
kadmin: Client 'kuser01/admin@EDT.ORG' not found in Kerberos database while initializing kadmin interface
@ kuser01 $ kadmin -p marta/admin
Password for marta/admin@EDT.ORG: 
kadmin:  listprincs
K/M@EDT.ORG
admin@EDT.ORG
anna@EDT.ORG
host/sshd.edt.org@EDT.ORG
jordi@EDT.ORG
julia@EDT.ORG
kadmin/admin@EDT.ORG
kadmin/changepw@EDT.ORG
kadmin/kserver.edt.org@EDT.ORG
kiprop/kserver.edt.org@EDT.ORG
krbtgt/EDT.ORG@EDT.ORG
kuser01@EDT.ORG
kuser02@EDT.ORG
kuser03@EDT.ORG
kuser04@EDT.ORG
kuser05@EDT.ORG
kuser06@EDT.ORG
marta/admin@EDT.ORG
marta@EDT.ORG
pau@EDT.ORG
pere@EDT.ORG
user01@EDT.ORG
user02@EDT.ORG
user03@EDT.ORG
user04@EDT.ORG
user05@EDT.ORG
user06@EDT.ORG
user07@EDT.ORG
user08@EDT.ORG
user09@EDT.ORG
user10@EDT.ORG
```

# Practica 2: Instal·lacio
Aqui vamos a necesitar tener otro ordenador de la aula pero en una nueva particion, para poder trabajar tranquilos de no dañar otras particiones.
En mi caso he instalado otro Debian 11.

En fin, hay que entrar en nuestra particion MATI y configurar el grub para tenerlo como estaba antes de la instalacion.
Dentro de MATI:
```
# cp /boot/grub/grub.cfg /boot/grub/grub.hisx2
# grub-mkconfig > /boot/grub/grub.cfg
# vim /boot/grub/grub.cfg

set default=0
set timeout=-1
MATI (posem aquesta etiqueta a la partició matí nvme0n1p5)
TARDA (posem aquesta etiqueta a la partició tarda nvme0n1p6)
HISX2-LAB (posem aquesta etiqueta a la partició de treball hisx2 nvme0n1p8)

# grub-install /dev/nvme0n1
```

# Practica 2: Host aula + Kerberos + AWS EC2
Aqui usaremos la particion que instalamos.
#### Necesario: AWS_EC2 + KServer
- [x] crear una nueva instacia (Maquina Amazon)
- [x] crear un nuevo security group = kerberos
- [x] abrir el kserver, del docker

1. entrar dentro de la maquina Amazon
1.1. bajar el kserver de nuestro docker hub
**Aqui no hace falta instalar el docker porque clone la maquina de otra que ya tenia el docker instalado.**
```
$ docker pull cristiancondolo21/krb22:kserver
```
2. encender el kserver con los puertos, en detach
```
$ docker run --rm --name kserver -h kserver.edt.org -p 88:88 -p 464:464 -p 749:749 -d cristiancondolo21/krb22:kserver
```

#### Necesario: Host aula + KClient
- [x] utilizar particion nueva (HISX2-LAB)
- [x] consegir docker y abrir el khost
- [x] editar /etc/hosts del khost
- [x] verificar funcionamiento

1. entrar dentro de HISX2-LAB como root
1.1. instalar el docker
```
# apt-get update

# apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

# curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

# echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null

# apt-get update

# apt-get install docker-ce docker-ce-cli containerd.io

# apt-get install docker-ce=5:20.10.13~3-0~debian-bullseye docker-ce-cli=5:20.10.13~3-0~debian-bullseye containerd.io
```
1.2. bajar el khost de nuestro docker hub
```
# docker pull cristiancondolo21/krb22:khost
```
2. encender el khost con los puerto abiertos
```
# docker run --rm --name khost -h khost.edt.org -p 88:88 -p 464:464 -p 749:749 -it cristiancondolo21/krb22:khost
```

#### Verificación
3. editar el /etc/hosts del khost, apunta la IP de la maquina Amazon
**Vigila que siempre que encendemos la maquina Amazon, la IP cambian.**
```d
<IP_AWS_Kserver>	kserver.edt.org kerver
``` 
4. comprobar autenticacion
```
@khost # kinit pere
Password for pere@EDT.ORG: 
@khost # klist
Ticket cache: FILE:/tmp/krb5cc_0
Default principal: pere@EDT.ORG

Valid starting     Expires            Service principal
03/17/22 11:20:06  03/17/22 21:20:06  krbtgt/EDT.ORG@EDT.ORG
	renew until 03/18/22 11:20:02
@khost # kdestroy
@khost # klist
klist: No credentials cache found (filename: /tmp/krb5cc_0)
```
5. comprobar roles de administracion
```
@khost # kadmin -p marta/admin
Authenticating as principal marta/admin with password.
Password for marta/admin@EDT.ORG: 
kadmin:  listprincs
K/M@EDT.ORG
admin@EDT.ORG
anna@EDT.ORG
host/sshd.edt.org@EDT.ORG
jordi@EDT.ORG
julia@EDT.ORG
kadmin/admin@EDT.ORG
kadmin/changepw@EDT.ORG
kadmin/kserver.edt.org@EDT.ORG
kiprop/kserver.edt.org@EDT.ORG
krbtgt/EDT.ORG@EDT.ORG
kuser01@EDT.ORG
kuser02@EDT.ORG
kuser03@EDT.ORG
kuser04@EDT.ORG
kuser05@EDT.ORG
kuser06@EDT.ORG
marta/admin@EDT.ORG
marta@EDT.ORG
pau@EDT.ORG
pere@EDT.ORG
user01@EDT.ORG
user02@EDT.ORG
user03@EDT.ORG
user04@EDT.ORG
user05@EDT.ORG
user06@EDT.ORG
user07@EDT.ORG
user08@EDT.ORG
user09@EDT.ORG
user10@EDT.ORG
```

# Práctica 3: Kerberos + LDAP (PAM)
#### Necesario: Khostpl
- [x] copiar los fichero de configuracion de los containers kserver y pam_ldap
- [x] editar el Dockerfile
- [x] editar el startup.sh
- [x] el servidor kerberos: kserver.edt.org y el servidor ldap(ldap_group): ldap.edt.org
- [x] configurar los common de PAM
- [x] encender los containers: kserver, ldap_group y khostpl
- [x] verifica los usuarios locales y globales(ldap+kerberos)
- [x] verificar usuarios locals (local01..) con passwd al kerberos y usuarios ldap (pere..., user01...)
- [x] los usuarios ldap tienen que tener password al kerberos (tipo kpere, kuser01, etc)

Dejamos el kserver tal como esta, tambien el ldap_group y nos centramos en configurar el khostpl.
1. añadir los archivos de pam-ldap y kserver
```
├── kadm5.acl
├── kdc.conf
├── krb5.conf
├── ldap.conf
├── login.defs
├── nslcd.conf
├── nsswitch.conf
```

2. editamos el Dockerfile
```dockerfile
FROM debian:11
LABEL version="1.0"
LABEL author="@edt ASIX-M06"
LABEL subject="Kerberos Client PAM_LDAP"
RUN apt-get update
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get -y install krb5-user libpam-krb5 mlocate procps iproute2 tree nmap nano vim less finger passwd libpam-pwquality libpam-mount libpam-mkhomedir libpam-ldapd libnss-ldapd nslcd nslcd-utils ldap-utils openssh-client openssh-server 
RUN mkdir /opt/docker
COPY . /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE 88 464 749
```

3. editamos el starup.sh
```bash
#! /bin/bash
# Khost-PAM
# @edt ASIX M11-SAD Curs 2021-2022

# Ficheros de configuracion
# Kerberos:
cp /opt/docker/krb5.conf /etc/krb5.conf # solamente este
#cp /opt/docker/kdc.conf  /etc/krb5kdc/kdc.conf
#cp /opt/docker/kadm5.acl /etc/krb5kdc/kadm5.acl

# PAM:
cp /opt/docker/common-auth /etc/pam.d/common-auth
cp /opt/docker/common-account /etc/pam.d/common-account
cp /opt/docker/common-password /etc/pam.d/common-password
cp /opt/docker/common-session /etc/pam.d/common-session

# PAM_LDAP
cp /opt/docker/ldap.conf /etc/ldap/ldap.conf
cp /opt/docker/login.defs /etc/login.defs
cp /opt/docker/nsswitch.conf /etc/nsswitch.conf
cp /opt/docker/nslcd.conf /etc/nslcd.conf

# Database (no hace falta)
#kdb5_util create -s -P masterkey # -P contraseña de la Database (masterkey)

# Creacio Grupos
groupadd localU
groupadd kusers

# Creacio Usuarios
useradd -g users -G localU local01
useradd -g users -G localU local02
useradd -g users -G localU local03

useradd -g users -G kusers kuser01
useradd -g users -G kusers kuser02
useradd -g users -G kusers kuser03

# Creacio Passwords
echo -e "local01\nlocal01" | passwd local01
echo -e "local02\nlocal02" | passwd local02
echo -e "local03\nlocal03" | passwd local03

# Encender servidores PAM_LDAP
/usr/sbin/nscd
/usr/sbin/nslcd

# Validar que funciona bien
getent passwd
getent group

/bin/bash
```
4. configurar los ficheros necesarios (common*):
    ```conf
    #
    # /etc/pam.d/common-account - authorization settings common to all services
    #
    # This file is included from other service-specific PAM config files,
    # and should contain a list of the authorization modules that define
    # the central access policy for use on the system.  The default is to
    # only deny service to users whose accounts are expired in /etc/shadow.
    #
    # As of pam 1.0.1-6, this file is managed by pam-auth-update by default.
    # To take advantage of this, it is recommended that you configure any
    # local modules either before or after the default block, and use
    # pam-auth-update to manage selection of other modules.  See
    # pam-auth-update(8) for details.
    #

    # here are the per-package modules (the "Primary" block)
    #account	[success=1 new_authtok_reqd=done default=ignore]	pam_unix.so 

    # ===Permisos Host-PAM_LDAP==============================================
    account     	sufficient    	pam_krb5.so
    account     	sufficient      pam_unix.so
    account	    	required	    pam_ldap.so	
    # =======================================================================

    # here's the fallback if no module succeeds
    #account	requisite			pam_deny.so

    # prime the stack with a positive return value if there isn't one already;
    # this avoids us returning an error just because nothing sets a success code
    # since the modules above will each just jump around
    #account	required			pam_permit.so

    # and here are more per-package modules (the "Additional" block)
    #account	[success=ok new_authtok_reqd=done ignore=ignore user_unknown=ignore authinfo_unavail=ignore default=bad]	pam_ldap.so minimum_uid=1000
    # end of pam-auth-update config

    #
    # /etc/pam.d/common-auth - authentication settings common to all services
    #
    # This file is included from other service-specific PAM config files,
    # and should contain a list of the authentication modules that define
    # the central authentication scheme for use on the system
    # (e.g., /etc/shadow, LDAP, Kerberos, etc.).  The default is to use the
    # traditional Unix authentication mechanisms.
    #
    # As of pam 1.0.1-6, this file is managed by pam-auth-update by default.
    # To take advantage of this, it is recommended that you configure any
    # local modules either before or after the default block, and use
    # pam-auth-update to manage selection of other modules.  See
    # pam-auth-update(8) for details.

    # here are the per-package modules (the "Primary" block)
    #auth	[success=2 default=ignore]	pam_unix.so nullok
    #auth	[success=1 default=ignore]	pam_ldap.so use_first_pass

    #===Autenticacio Host-PAM_LDAP========================================
    auth        	required      	pam_env.so
    auth        	sufficient    	pam_unix.so try_first_pass nullok
    auth		sufficient	pam_ldap.so
    auth        	sufficient    	pam_krb5.so 
    auth        	required      	pam_deny.so
    #=====================================================================

    # here's the fallback if no module succeeds
    #auth	requisite			pam_deny.so

    # prime the stack with a positive return value if there isn't one already;
    # this avoids us returning an error just because nothing sets a success code
    # since the modules above will each just jump around
    #auth	required			pam_permit.so

    # and here are more per-package modules (the "Additional" block)
    #auth	optional	pam_mount.so 
    #auth	optional			pam_cap.so 
    # end of pam-auth-update config

    #
    # /etc/pam.d/common-password - password-related modules common to all services
    #
    # This file is included from other service-specific PAM config files,
    # and should contain a list of modules that define the services to be
    # used to change user passwords.  The default is pam_unix.

    # Explanation of pam_unix options:
    # The "yescrypt" option enables
    #hashed passwords using the yescrypt algorithm, introduced in Debian
    #11.  Without this option, the default is Unix crypt.  Prior releases
    #used the option "sha512"; if a shadow password hash will be shared
    #between Debian 11 and older releases replace "yescrypt" with "sha512"
    #for compatibility .  The "obscure" option replaces the old
    #`OBSCURE_CHECKS_ENAB' option in login.defs.  See the pam_unix manpage
    #for other options.

    # As of pam 1.0.1-6, this file is managed by pam-auth-update by default.
    # To take advantage of this, it is recommended that you configure any
    # local modules either before or after the default block, and use
    # pam-auth-update to manage selection of other modules.  See
    # pam-auth-update(8) for details.

    # here are the per-package modules (the "Primary" block)
    #password	requisite			pam_pwquality.so retry=3
    #password	[success=2 default=ignore]	pam_unix.so obscure use_authtok try_first_pass yescrypt
    #password	[success=1 default=ignore]	pam_ldap.so minimum_uid=1000 try_first_pass

    #==Passwords Host-PAM_LDAP========================================================================
    password	requisite	pam_pwquality.so retry=3
    password    	sufficient    	pam_unix.so try_first_pass use_authtok nullok sha512 shadow
    password        sufficient	pam_ldap.so
    password    	sufficient    	pam_krb5.so
    password    	required      	pam_deny.so
    #password	required	pam_permit.so
    #password	optional	pam_mount.so disable_interactive
    #=================================================================================================

    # here's the fallback if no module succeeds
    #password	requisite			pam_deny.so

    # prime the stack with a positive return value if there isn't one already;
    # this avoids us returning an error just because nothing sets a success code
    # since the modules above will each just jump around
    #password	required			pam_permit.so

    # and here are more per-package modules (the "Additional" block)
    #password	optional	pam_mount.so disable_interactive
    # end of pam-auth-update config
    
    #
    # /etc/pam.d/common-session - session-related modules common to all services
    #
    # This file is included from other service-specific PAM config files,
    # and should contain a list of modules that define tasks to be performed
    # at the start and end of interactive sessions.
    #
    # As of pam 1.0.1-6, this file is managed by pam-auth-update by default.
    # To take advantage of this, it is recommended that you configure any
    # local modules either before or after the default block, and use
    # pam-auth-update to manage selection of other modules.  See
    # pam-auth-update(8) for details.


    # here are the per-package modules (the "Primary" block)
    #session	[default=1]			pam_permit.so

    # here's the fallback if no module succeeds
    #session	requisite			pam_deny.so

    # prime the stack with a positive return value if there isn't one already;
    # this avoids us returning an error just because nothing sets a success code
    # since the modules above will each just jump around
    #session	required			pam_permit.so

    #====Sesions Host-PAM_LDAP========================================================
    session     	optional	pam_keyinit.so revoke
    session     	required      	pam_limits.so
    -session     	optional      	pam_systemd.so
    session     	[success=1 default=ignore] 	pam_succeed_if.so service in crond quiet use_uid
    session		required	pam_ldap.so
    session 	optional 	pam_mkhomedir.so
    session     	sufficient    	pam_krb5.so
    session		sufficient	pam_unix.so 
    #session		optional	pam_mount.so
    #=================================================================================

    # and here are more per-package modules (the "Additional" block)
    #session		required	pam_unix.so 
    #session		optional	pam_mount.so
    #session 	optional 	pam_mkhomedir.so
    #session	[success=ok default=ignore]	pam_ldap.so minimum_uid=1000
    # end of pam-auth-update config
    ```

5. montar la imagen
```
$ sudo docker build -t cristiancondolo21/krb22:khostpl .
```

#### Verificacion
6. encender los containers (kserver, ldap_group y khostp)
```
$ sudo docker run --rm --name kserver -h kserver.edt.org --net 2hisix -d cristiancondolo21/krb22:kserver
$ sudo docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -d cristiancondolo21/ldap21:grups
$ sudo docker run --rm --name khostldap.edt.org --hostname khostldap.edt.org --net 2hisix --privileged -it cristiancondolo21/krb22:khostpl
```

7. hacer una consulta
```
# ldapsearch -x -LLL | head
dn: dc=edt,dc=org
dc: edt
description: Escola del treball de Barcelona
objectClass: dcObject
objectClass: organization
o: edt.org

dn: ou=maquines,dc=edt,dc=org
ou: maquines
description: Container per a maquines linux
```

8. revisar los usuarios
```
# getent passwd
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
_apt:x:100:65534::/nonexistent:/usr/sbin/nologin
systemd-timesync:x:101:101:systemd Time Synchronization,,,:/run/systemd:/usr/sbin/nologin
systemd-network:x:102:103:systemd Network Management,,,:/run/systemd:/usr/sbin/nologin
systemd-resolve:x:103:104:systemd Resolver,,,:/run/systemd:/usr/sbin/nologin
messagebus:x:104:105::/nonexistent:/usr/sbin/nologin
nslcd:x:105:107:nslcd name service LDAP connection daemon,,,:/var/run/nslcd/:/usr/sbin/nologin
sshd:x:106:65534::/run/sshd:/usr/sbin/nologin
local01:x:1000:100::/home/local01:/bin/sh
local02:x:1001:100::/home/local02:/bin/sh
local03:x:1002:100::/home/local03:/bin/sh
kuser01:x:1003:100::/home/kuser01:/bin/sh
kuser02:x:1004:100::/home/kuser02:/bin/sh
kuser03:x:1005:100::/home/kuser03:/bin/sh
pau:*:5000:100:Pau Pou:/tmp/home/pau:
pere:*:5001:100:Pere Pou:/tmp/home/pere:
anna:*:5002:600:Anna Pou:/tmp/home/anna:
marta:*:5003:600:Marta Mas:/tmp/home/marta:
jordi:*:5004:100:Jordi Mas:/tmp/home/jordi:
admin:*:10:10:Administrador Sistema:/tmp/home/admin:
user01:*:7001:610:user01:/tmp/home/1asix/user01:
user02:*:7002:610:user02:/tmp/home/1asix/user02:
user02:*:7003:610:user03:/tmp/home/1asix/user03:
user03:*:7003:610:user03:/tmp/home/1asix/user03:
user04:*:7004:610:user04:/tmp/home/1asix/user04:
user05:*:7005:610:user05:/tmp/home/1asix/user05:
user06:*:7006:611:user06:/tmp/home/2asix/user06:
user07:*:7007:611:user07:/tmp/home/2asix/user07:
user08:*:7008:611:user08:/tmp/home/2asix/user08:
user09:*:7009:611:user09:/tmp/home/2asix/user09:
user10:*:7010:611:user10:/tmp/home/2asix/user10:
new01:*:7020:613:new01:/tmp/home/2hiaw/new01:
new02:*:7021:614:new02:/tmp/home/2hiaw/new02:
```

9.  revisar los grupos
```
# getent group
root:x:0:
daemon:x:1:
bin:x:2:
sys:x:3:
adm:x:4:
tty:x:5:
disk:x:6:
lp:x:7:
mail:x:8:
news:x:9:
uucp:x:10:
man:x:12:
proxy:x:13:
kmem:x:15:
dialout:x:20:
fax:x:21:
voice:x:22:
cdrom:x:24:
floppy:x:25:
tape:x:26:
sudo:x:27:
audio:x:29:
dip:x:30:
www-data:x:33:
backup:x:34:
operator:x:37:
list:x:38:
irc:x:39:
src:x:40:
gnats:x:41:
shadow:x:42:
utmp:x:43:
video:x:44:
sasl:x:45:
plugdev:x:46:
staff:x:50:
games:x:60:
users:x:100:
nogroup:x:65534:
systemd-timesync:x:101:
systemd-journal:x:102:
systemd-network:x:103:
systemd-resolve:x:104:
messagebus:x:105:
mlocate:x:106:
nslcd:x:107:
ssh:x:108:
localU:x:1000:local01,local02,local03
kusers:x:1001:kuser01,kuser02,kuser03
```

10. entrar como los usuario con passwd y los sin passwd
```
# su - local01
Creating directory '/home/local01'.
$ pwd
/home/local01
$ su - local02
Password: 
Creating directory '/home/local02'.
$ su - local01
Password: 
$ su -l pere
Password: 
Creating directory '/tmp/home/pere'.
```

11. obtener un ticker(krb)  
```
$ kinit pere
Password for pere@EDT.ORG: 
$
```

    11.1. mostrar ticket
    ```
    $ klist
    Ticket cache: FILE:/tmp/krb5cc_5001
    Default principal: pere@EDT.ORG

    Valid starting     Expires            Service principal
    03/24/22 18:39:38  03/25/22 04:39:38  krbtgt/EDT.ORG@EDT.ORG
        renew until 03/25/22 18:39:33
    ```

12. entra a kadmin
```
$ kadmin      
Authenticating as principal pere/admin@EDT.ORG with password.
kadmin: Client 'pere/admin@EDT.ORG' not found in Kerberos database while initializing kadmin interface
$ kadmin -p marta/admin
Authenticating as principal marta/admin with password.
Password for marta/admin@EDT.ORG: 
kadmin:  
```

    12.1. hacer un listprincs
    ```
    kadmin:  listprincs
    K/M@EDT.ORG
    admin@EDT.ORG
    anna@EDT.ORG
    host/sshd.edt.org@EDT.ORG
    jordi@EDT.ORG
    julia@EDT.ORG
    kadmin/admin@EDT.ORG
    kadmin/changepw@EDT.ORG
    kadmin/kserver.edt.org@EDT.ORG
    kiprop/kserver.edt.org@EDT.ORG
    krbtgt/EDT.ORG@EDT.ORG
    kuser01@EDT.ORG
    kuser02@EDT.ORG
    kuser03@EDT.ORG
    kuser04@EDT.ORG
    kuser05@EDT.ORG
    kuser06@EDT.ORG
    marta/admin@EDT.ORG
    marta@EDT.ORG
    pau@EDT.ORG
    pere@EDT.ORG
    user01@EDT.ORG
    user02@EDT.ORG
    user03@EDT.ORG
    user04@EDT.ORG
    user05@EDT.ORG
    user06@EDT.ORG
    user07@EDT.ORG
    user08@EDT.ORG
    user09@EDT.ORG
    user10@EDT.ORG
    ```

13. conseguir ticker a un usuario ldap
```
$ kinit kuser01
Password for kuser01@EDT.ORG: 
$ klist
Ticket cache: FILE:/tmp/krb5cc_5001
Default principal: kuser01@EDT.ORG

Valid starting     Expires            Service principal
03/24/22 18:44:37  03/25/22 04:44:37  krbtgt/EDT.ORG@EDT.ORG
	renew until 03/25/22 18:44:32
```

# Práctica 3: Host Aula + Kerberos + LDAP + AWS EC2
#### Necesario: AWS_EC2 + Kserver + PAM_Ldap
- [ ] crear una nueva instacia (Maquina Amazon)
- [ ] crear un nuevo security group = kerberos_ldap
- [ ] abrir el kserver y pam_ldap del docker

#### Necesario: Host + Khostpl
- [ ] utilizar particion nueva (HISX2-LAB)
- [ ] abrir el khostpl
- [ ] editar /etc/hosts del khost
- [ ] verificar funcionamiento

# Práctica4: Servei SSH Kerberitzat Bàsic
#### Necesario: sshd
- [ ] copiar los ficheros de khost, como base
- [ ] añadir los ficheros de configuracion del ssh
- [ ] editar el Dockerfile
- [ ] editar el startup.sh
- [ ] editar ssh_config
- [ ] editar sshd_config

1. añadir los fichero necesarios
2. editamos los fichero principales: Dockerfile y startup
3. configurar el servidor ssh
4. montar la imagen

#### Verificación
5. encender los containers necesarios, en detach
6. entrar dentro del sshd
7. comprobar funcionamiento del servidor ssh

# Práctica 4: Servei SSH Kerberitzat (Kerberos + LDAP)
#### Necesario: sshdpl

#### Verificación

# Práctica 4: Desplegament SSH a AWS EC2
#### Necesario: AWS_EC2 + kserver + sshd

#### Verificación

#### Necesario: ssh_client

#### Verificación

# Práctica 4: Desplegament SSH_LDAP a AWS EC2
#### Necesario: AWS_EC2 + kserver + sshd + ldap_group

#### Verificación

#### Necesario: sshdpl

#### Verificación