#! /bin/bash
# Ficheros de configuracion
cp /opt/docker/krb5.conf /etc/krb5.conf
cp /opt/docker/ssh_config /etc/ssh/ssh_config
cp /opt/docker/hosts /etc/hosts

# Generamos la CLAVE de HOST - Iniciamos com marta - kmarta i hacemos kadmin
#kadmin -p admin -w kadmin -q "ktadd -k /etc/krb5.keytab host/sshd.edt.org"

# Creación usuario con passwd (local0) y los usuario sin passwd (kuser0..,anna, pere, marta, jordi, pau)
for user in local01 local02 local03
do
  useradd $user
  echo -e "$user\n$user\n" | passwd $user
done

for user in anna pere marta jordi pau kuser01 kuser02 kuser03 kuser04 kuser05 kuser06
do
  useradd $user
done

# Encender el servidor ssh
mkdir /run/sshd
/usr/sbin/sshd && echo "SSH Activado"
# En detach
/usr/sbin/sshd -D && echo "SSH Activado"

/bin/bash
