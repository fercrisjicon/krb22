#! /bin/bash
# Khost-PAM
# @edt ASIX M11-SAD Curs 2021-2022

# Ficheros de configuracion
# Kerberos:
cp /opt/docker/krb5.conf /etc/krb5.conf # solamente este
#cp /opt/docker/kdc.conf  /etc/krb5kdc/kdc.conf
#cp /opt/docker/kadm5.acl /etc/krb5kdc/kadm5.acl

# PAM:
cp /opt/docker/common-auth /etc/pam.d/common-auth
cp /opt/docker/common-account /etc/pam.d/common-account
cp /opt/docker/common-password /etc/pam.d/common-password
cp /opt/docker/common-session /etc/pam.d/common-session

# PAM_LDAP
cp /opt/docker/ldap.conf /etc/ldap/ldap.conf
cp /opt/docker/login.defs /etc/login.defs
cp /opt/docker/nsswitch.conf /etc/nsswitch.conf
cp /opt/docker/nslcd.conf /etc/nslcd.conf

# Database (no hace falta)
#kdb5_util create -s -P masterkey # -P contraseña de la Database (masterkey)

# Creacio Grupos
groupadd localU
groupadd kusers

# Creacio Usuarios
useradd -g users -G localU local01
useradd -g users -G localU local02
useradd -g users -G localU local03

useradd -g users -G kusers kuser01
useradd -g users -G kusers kuser02
useradd -g users -G kusers kuser03

# Creacio Passwords
echo -e "local01\nlocal01" | passwd local01
echo -e "local02\nlocal02" | passwd local02
echo -e "local03\nlocal03" | passwd local03

# Encender servidores PAM_LDAP
/usr/sbin/nscd
/usr/sbin/nslcd

# Validar que funciona bien
getent passwd
getent group

/bin/bash
